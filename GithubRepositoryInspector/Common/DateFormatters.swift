//
//  DateFormatters.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/15/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import Foundation

class DateFormatters {
    
    static var iso8601dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        return dateFormatter
    }
    
    static var shortDateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        return dateFormatter
    }
}
