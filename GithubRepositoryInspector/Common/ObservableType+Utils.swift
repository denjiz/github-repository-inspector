//
//  ObservableType+Utils.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/15/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import RxSwift

extension ObservableType {
    
    func mapVoid() -> Observable<Void> {
        return map { _ in }
    }
}
