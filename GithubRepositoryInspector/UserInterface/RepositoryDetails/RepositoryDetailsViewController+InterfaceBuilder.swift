//
//  RepositoryDetailsViewController+InterfaceBuilder.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/16/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import PureLayout

extension RepositoryDetailsViewController {
    
    func buildInterface() {
        view = UIView()
        view.backgroundColor = .white
        
        let scrollView = UIScrollView()
        view.addSubview(scrollView)
        scrollView.autoPinEdgesToSuperviewEdges()
        
        let mainStackView = createVerticalStackView(spacing: 40, margin: 10)
        scrollView.addSubview(mainStackView)
        mainStackView.autoPinEdgesToSuperviewEdges()
        mainStackView.autoMatch(.width, to: .width, of: view)
        
        let repositoryStackView = createRepositoryStackView()
        mainStackView.addArrangedSubview(repositoryStackView)
        repositoryStackView.autoMatch(.width, to: .width, of: mainStackView)
        
        let ownerStackView = createOwnerStackView()
        mainStackView.addArrangedSubview(ownerStackView)
        ownerStackView.autoMatch(.width, to: .width, of: mainStackView)
    }
    
    private func createVerticalStackView(spacing: CGFloat, margin: CGFloat) -> UIStackView {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = spacing
        stackView.alignment = .center
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = UIEdgeInsets(top: margin, left: 0, bottom: margin, right: 0)
        return stackView
    }
    
    private func createRepositoryStackView() -> UIStackView {
        let stackView = createVerticalStackView(spacing: 15, margin: 0)
        
        let repositoryHeaderLabel = createHeaderLabel()
        stackView.addArrangedSubview(repositoryHeaderLabel)
        repositoryHeaderLabel.text = "Repository"
        
        repositoryNameView = addKeyValueView(to: stackView, key: "Name")
        repositoryDescriptionView = addKeyValueView(to: stackView, key: "Description")
        repositoryDateCreatedView = addKeyValueView(to: stackView, key: "Created")
        repositoryDateUpdatedView = addKeyValueView(to: stackView, key: "Updated")
        repositoryLanguageView = addKeyValueView(to: stackView, key: "Language")
        repositoryWatchersView = addKeyValueView(to: stackView, key: "Watchers")
        repositoryForksView = addKeyValueView(to: stackView, key: "Forks")
        repositoryOpenIssuesView = addKeyValueView(to: stackView, key: "Open issues")
        
        visitRepositoryButton = createLinkButton(title: "Visit repository")
        stackView.addArrangedSubview(visitRepositoryButton)
        
        return stackView
    }
    
    private func createOwnerStackView() -> UIStackView {
        let stackView = createVerticalStackView(spacing: 15, margin: 0)
        
        let ownerHeaderLabel = createHeaderLabel()
        stackView.addArrangedSubview(ownerHeaderLabel)
        ownerHeaderLabel.text = "Owner"
        
        ownerImageView = HanekeImageView()
        stackView.addArrangedSubview(ownerImageView)
        ownerImageView.autoSetDimensions(to: CGSize(width: 150, height: 150))
        
        ownerUsernameView = addKeyValueView(to: stackView, key: "Username")
        
        visitOwnerButton = createLinkButton(title: "View profile")
        stackView.addArrangedSubview(visitOwnerButton)
        
        return stackView
    }
    
    private func createHeaderLabel() -> UILabel {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 30)
        return label
    }
    
    private func addKeyValueView(to stackView: UIStackView, key: String) -> KeyValueView {
        let keyValueView = KeyValueView()
        stackView.addArrangedSubview(keyValueView)
        keyValueView.keyLabel.text = key
        return keyValueView
    }
    
    private func createLinkButton(title: String) -> UIButton {
        let button = UIButton()
        let titleAttributes: [String : Any] = [NSUnderlineStyleAttributeName : NSUnderlineStyle.styleSingle.rawValue,
                                              NSForegroundColorAttributeName : UIColor.blue]
        let attributedTitle = NSAttributedString(string: title, attributes: titleAttributes)
        button.setAttributedTitle(attributedTitle, for: .normal)
        
        return button
    }
}
