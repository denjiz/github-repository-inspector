//
//  RepositoryDetailsViewController.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/16/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import UIKit
import RxSwift

class RepositoryDetailsViewController: UIViewController {
    
    var repositoryNameView: KeyValueView!
    var repositoryDescriptionView: KeyValueView!
    var repositoryDateCreatedView: KeyValueView!
    var repositoryDateUpdatedView: KeyValueView!
    var repositoryLanguageView: KeyValueView!
    var repositoryWatchersView: KeyValueView!
    var repositoryForksView: KeyValueView!
    var repositoryOpenIssuesView: KeyValueView!
    var visitRepositoryButton: UIButton!
    
    var ownerImageView: HanekeImageView!
    var ownerUsernameView: KeyValueView!
    var visitOwnerButton: UIButton!
    
    private let disposeBag = DisposeBag()
    private var viewModel: RepositoryDetailsViewModel!
    
    convenience init(viewModel: RepositoryDetailsViewModel) {
        self.init()
        self.viewModel = viewModel
    }
    
    override func loadView() {
        buildInterface()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        connectWithViewModel()
    }
    
    private func setupNavigationBar() {
        title = viewModel.repository.name
    }
    
    private func connectWithViewModel() {
        let repository = viewModel.repository
        
        repositoryNameView.valueLabel.text = repository.name
        repositoryDescriptionView.valueLabel.text = repository.description
        repositoryDateCreatedView.valueLabel.text = DateFormatters.shortDateFormatter.string(from: repository.dateCreated)
        repositoryDateUpdatedView.valueLabel.text = DateFormatters.shortDateFormatter.string(from: repository.dateUpdated)
        repositoryLanguageView.valueLabel.text = repository.language
        repositoryWatchersView.valueLabel.text = String(repository.numberOfWatchers)
        repositoryForksView.valueLabel.text = String(repository.numberOfForks)
        repositoryOpenIssuesView.valueLabel.text = String(repository.numberOfOpenIssues)
        
        ownerImageView.imageUrl = URL(string: repository.owner.avatarUrlString)
        ownerUsernameView.valueLabel.text = repository.owner.username
        
        visitRepositoryButton.rx
            .tap
            .bind(onNext: viewModel.visitRepositoryTapped)
            .addDisposableTo(disposeBag)
        
        visitOwnerButton.rx
            .tap
            .bind(onNext: viewModel.visitOwnerTapped)
            .addDisposableTo(disposeBag)
    }
}
