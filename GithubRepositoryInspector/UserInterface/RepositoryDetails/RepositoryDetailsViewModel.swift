//
//  RepositoryDetailsViewModel.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/16/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import Foundation

class RepositoryDetailsViewModel {
    
    let repository: Repository
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies, repository: Repository) {
        self.dependencies = dependencies
        self.repository = repository
    }
    
    func visitRepositoryTapped() {
        dependencies.navigationService.openLink(with: repository.urlString)
    }
    
    func visitOwnerTapped() {
        dependencies.navigationService.openLink(with: repository.owner.urlString)
    }
}

extension RepositoryDetailsViewModel {
    
    struct Dependencies {
        let navigationService: NavigationService
    }
}
