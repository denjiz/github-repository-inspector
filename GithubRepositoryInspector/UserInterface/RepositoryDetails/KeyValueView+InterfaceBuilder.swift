//
//  KeyValueView+InterfaceBuilder.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/16/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import PureLayout

extension KeyValueView {
    
    func buildInterface() {
        keyLabel = UILabel()
        addSubview(keyLabel)
        keyLabel.autoAlignAxis(toSuperviewAxis: .vertical)
        keyLabel.autoPinEdge(toSuperviewEdge: .top)
        keyLabel.font = UIFont.boldSystemFont(ofSize: 18)
        
        valueLabel = UILabel()
        addSubview(valueLabel)
        valueLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 20)
        valueLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 20)
        valueLabel.autoPinEdge(.top, to: .bottom, of: keyLabel, withOffset: 5)
        valueLabel.autoPinEdge(toSuperviewEdge: .bottom)
        valueLabel.numberOfLines = 0
        valueLabel.textAlignment = .center
    }
}
