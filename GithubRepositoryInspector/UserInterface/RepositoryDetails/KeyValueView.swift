//
//  KeyValueView.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/16/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import UIKit

class KeyValueView: UIView {
    
    var keyLabel: UILabel!
    var valueLabel: UILabel!
    
    init() {
        super.init(frame: .zero)
        buildInterface()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        buildInterface()
    }
}
