//
//  HanekeImageView.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/15/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import Haneke

class HanekeImageView: UIImageView {
    
    var imageUrl: URL? {
        didSet {
            image = nil
            if self.frame.size == .zero {
                setNeedsLayout()
            } else if let imageUrl = imageUrl {
                hnk_setImage(from: imageUrl)
            }
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let imageUrl = imageUrl, self.frame.size != .zero {
            hnk_setImage(from: imageUrl)
        }
    }
}
