//
//  NavigationService.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/14/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import UIKit
import RxSwift

class NavigationService {
    
    private let navigationController = UINavigationController()
    private let apiController: APIController
    private let window: UIWindow!
    
    init(window: UIWindow) {
        self.window = window
        
        navigationController.navigationBar.isTranslucent = false
        
        let apiControllerDependencies = APIController.Dependencies(urlSession: URLSession.shared)
        apiController = APIController(dependencies: apiControllerDependencies)
    }
    
    func displayInitialViewController() {
        let dependencies = SearchRepositoriesViewModel.Dependencies(navigationService: self, apiController: apiController)
        let viewModel = SearchRepositoriesViewModel(dependencies: dependencies)
        let viewController = SearchRepositoriesViewController(viewModel: viewModel)
        
        navigationController.viewControllers = [viewController]
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
    }
    
    func presentSortTypeAlertController() -> Observable<SortType> {
        return Observable<SortType>.create { [weak self] observer in
            guard let `self` = self else { return Disposables.create() }
            
            let alertController = UIAlertController(title: "Sort by:", message: nil, preferredStyle: .actionSheet)
            
            let sortActions = SortType.allValues.map { sortType in
                UIAlertAction(title: sortType.rawValue.capitalized, style: .default) { _ in
                    observer.onNext(sortType)
                }
            }
            sortActions.forEach { alertController.addAction($0) }
            
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel)
            alertController.addAction(cancelAction)
            
            self.navigationController.present(alertController, animated: true)
            
            return Disposables.create()
        }
    }
    
    func pushRepositoryDetailsViewController(for repository: Repository) {
        let dependencies = RepositoryDetailsViewModel.Dependencies(navigationService: self)
        let viewModel = RepositoryDetailsViewModel(dependencies: dependencies, repository: repository)
        let viewController = RepositoryDetailsViewController(viewModel: viewModel)
        navigationController.pushViewController(viewController, animated: true)
    }
    
    func openLink(with urlString: String) {
        if let url = URL(string: urlString) {
            UIApplication.shared.openURL(url)
        }
    }
}
