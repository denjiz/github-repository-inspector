//
//  LoadingFooterView.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/15/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import UIKit

class LoadingFooterView: UIView {
    
    enum Style {
        case empty
        case loading
        case error(String)
    }
    
    var activityIndicator: UIActivityIndicatorView!
    var errorLabel: UILabel!
    
    init() {
        super.init(frame: .zero)
        buildInterface()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        buildInterface()
    }
    
    func setStyle(_ style: Style) {
        switch style {
        case .empty:
            activityIndicator.isHidden = true
            activityIndicator.stopAnimating()
            errorLabel.isHidden = true
        case .loading:
            activityIndicator.isHidden = false
            activityIndicator.startAnimating()
            errorLabel.isHidden = true
        case .error(let message):
            errorLabel.isHidden = false
            errorLabel.text = message
            activityIndicator.isHidden = true
            activityIndicator.stopAnimating()
        }
    }
}
