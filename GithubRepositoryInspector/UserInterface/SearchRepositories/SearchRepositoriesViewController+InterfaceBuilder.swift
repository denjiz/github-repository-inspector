//
//  SearchRepositoriesViewController+InterfaceBuilder.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/14/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import PureLayout

extension SearchRepositoriesViewController {
    
    func buildInterface() {
        view = UIView()
        view.backgroundColor = .white
        
        searchBar = UISearchBar()
        view.addSubview(searchBar)
        searchBar.autoPinEdge(toSuperviewEdge: .left)
        searchBar.autoPinEdge(toSuperviewEdge: .right)
        searchBar.autoPinEdge(toSuperviewEdge: .top)
        
        sortTypeView = UIView()
        view.insertSubview(sortTypeView, belowSubview: searchBar)
        sortTypeView.autoPinEdge(toSuperviewEdge: .left)
        sortTypeView.autoPinEdge(toSuperviewEdge: .right)
        sortTypeView.autoSetDimension(.height, toSize: 50)
        sortTypeViewTopConstraint = sortTypeView.autoPinEdge(.top, to: .bottom, of: searchBar, withOffset: -50)
        sortTypeView.backgroundColor = UIColor(white: 0.8, alpha: 1)
        
        sortTypeLabel = UILabel()
        sortTypeView.addSubview(sortTypeLabel)
        sortTypeLabel.autoPinEdge(toSuperviewEdge: .left, withInset: 10)
        sortTypeLabel.autoAlignAxis(toSuperviewAxis: .horizontal)
        
        changeSortTypeButton = UIButton()
        sortTypeView.addSubview(changeSortTypeButton)
        changeSortTypeButton.autoPinEdge(toSuperviewEdge: .right, withInset: 10)
        changeSortTypeButton.autoAlignAxis(toSuperviewAxis: .horizontal)
        changeSortTypeButton.setTitle("Change...", for: .normal)
        changeSortTypeButton.setTitleColor(.blue, for: .normal)
        
        tableView = UITableView(frame: .zero, style: .grouped)
        view.insertSubview(tableView, belowSubview: sortTypeView)
        tableView.autoPinEdge(toSuperviewEdge: .left)
        tableView.autoPinEdge(toSuperviewEdge: .right)
        tableView.autoPinEdge(.top, to: .bottom, of: searchBar)
        tableView.autoPinEdge(toSuperviewEdge: .bottom)
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        view.addSubview(activityIndicator)
        activityIndicator.autoCenterInSuperview()
        
        errorView = UIStackView()
        view.addSubview(errorView)
        errorView.autoCenterInSuperview()
        errorView.axis = .vertical
        errorView.distribution = .equalSpacing
        errorView.spacing = 10
        errorView.alignment = .center
        
        errorLabel = UILabel()
        errorView.addArrangedSubview(errorLabel)
        errorLabel.autoMatch(.width, to: .width, of: view, withOffset: -100)
        errorLabel.numberOfLines = 0
        errorLabel.textAlignment = .center
        
        tryAgainButton = UIButton()
        errorView.addArrangedSubview(tryAgainButton)
        tryAgainButton.setTitle("Try Again", for: .normal)
        tryAgainButton.contentEdgeInsets = UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
        tryAgainButton.backgroundColor = .black
        
        loadingFooterView = LoadingFooterView()
    }
}
