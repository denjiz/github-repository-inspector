//
//  RepositoryTableViewCell.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/14/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import UIKit
import Haneke

class RepositoryTableViewCell: UITableViewCell {
    
    var rootView: UIView!
    var ownerImageView: HanekeImageView!
    var ownerUsernameLabel: UILabel!
    var repositoryNameLabel: UILabel!
    var infoStackView: UIStackView!
    var dateUpdatedLabel: UILabel!
    var watchersLabel: UILabel!
    var forksLabel: UILabel!
    var openIssuesLabel: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildInterface()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        buildInterface()
    }
    
    func setup(with repository: Repository) {
        ownerImageView.imageUrl = URL(string: repository.owner.avatarUrlString)
        ownerUsernameLabel.text = "Owner: \(repository.owner.username)"
        repositoryNameLabel.text = "Repository: \(repository.name)"
        watchersLabel.text = "Watchers: \(repository.numberOfWatchers)"
        forksLabel.text = "Forks: \(repository.numberOfForks)"
        openIssuesLabel.text = "Open issues: \(repository.numberOfOpenIssues)"
        dateUpdatedLabel.text = "Updated: \(DateFormatters.shortDateFormatter.string(from: repository.dateUpdated))"
    }
}
