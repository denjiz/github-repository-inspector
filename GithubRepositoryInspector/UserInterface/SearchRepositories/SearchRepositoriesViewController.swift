//
//  SearchRepositoriesViewController.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/14/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchRepositoriesViewController: UIViewController {
    
    var searchBar: UISearchBar!
    var sortTypeView: UIView!
    var sortTypeLabel: UILabel!
    var changeSortTypeButton: UIButton!
    var tableView: UITableView!
    var activityIndicator: UIActivityIndicatorView!
    var errorView: UIStackView!
    var errorLabel: UILabel!
    var tryAgainButton: UIButton!
    var loadingFooterView: LoadingFooterView!
    
    var sortTypeViewTopConstraint: NSLayoutConstraint!
    
    fileprivate var viewModel: SearchRepositoriesViewModel!
    private let disposeBag = DisposeBag()
    
    convenience init(viewModel: SearchRepositoriesViewModel) {
        self.init()
        self.viewModel = viewModel
    }
    
    override func loadView() {
        buildInterface()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationBar()
        setupTableView()
        connectWithViewModel()
    }
    
    private func setupNavigationBar() {
        title = "Search Repositories"
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    private func setupTableView() {
        tableView.register(RepositoryTableViewCell.self,
                           forCellReuseIdentifier: String(describing: RepositoryTableViewCell.self))
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 200
    }
    
    private func connectWithViewModel() {
        viewModel
            .repositories
            .asObservable()
            .mapVoid()
            .bind(onNext: tableView.reloadData)
            .addDisposableTo(disposeBag)
        
        viewModel
            .sortType
            .asObservable()
            .subscribe(onNext: { [weak self] sortType in
                self?.sortTypeLabel.text = "Sort by: \(sortType.rawValue.capitalized)"
            })
            .addDisposableTo(disposeBag)
        
        viewModel
            .isLoadingNewRepositories
            .asObservable()
            .subscribe(onNext: { [weak self] isLoadingNewRepositories in
                self?.activityIndicator.isHidden = !isLoadingNewRepositories
                isLoadingNewRepositories ?
                    self?.activityIndicator.startAnimating() :
                    self?.activityIndicator.stopAnimating()
            })
            .addDisposableTo(disposeBag)
        
        viewModel
            .newRepositoriesLoadingError
            .asObservable()
            .subscribe(onNext: { [weak self] errorMessage in
                self?.errorLabel.text = errorMessage
                self?.errorView.isHidden = errorMessage == nil
            })
            .addDisposableTo(disposeBag)
        
        viewModel
            .isLoadingMoreRepositories
            .asObservable()
            .subscribe(onNext: { [weak self] isLoadingMoreRepositories in
                let footerStyle: LoadingFooterView.Style = isLoadingMoreRepositories ? .loading : .empty
                self?.loadingFooterView.setStyle(footerStyle)
            })
            .addDisposableTo(disposeBag)
        
        viewModel
            .moreRepositoriesLoadingError
            .asObservable()
            .subscribe(onNext: { [weak self] errorMessage in
                let footerStyle: LoadingFooterView.Style = errorMessage.flatMap { .error($0) } ?? .empty
                self?.loadingFooterView.setStyle(footerStyle)
            })
            .addDisposableTo(disposeBag)
        
        Observable
            .merge(searchBar.rx.textDidBeginEditing.map { true },
                   searchBar.rx.textDidEndEditing.map { false })
            .bind(onNext: toggleSortTypeView)
            .addDisposableTo(disposeBag)
        
        Observable
            .merge(searchBar.rx.searchButtonClicked.asObservable(),
                   tryAgainButton.rx.tap.asObservable())
            .do(onNext: { [weak self] in self?.searchBar.resignFirstResponder() })
            .withLatestFrom(searchBar.rx.value)
            .map { $0 ?? "" }
            .bind(onNext: viewModel.searchRequested)
            .addDisposableTo(disposeBag)
        
        changeSortTypeButton.rx
            .tap
            .bind(onNext: viewModel.changeSortTypeTapped)
            .addDisposableTo(disposeBag)
        
        tableView.rx
            .willDisplayCell
            .subscribe(onNext: { [weak self] event in
                guard let `self` = self else { return }
                let isCellLast = event.indexPath.row == self.tableView.numberOfRows(inSection: 0) - 1
                if isCellLast {
                    self.viewModel.scrolledToEndOfList()
                }
            })
            .addDisposableTo(disposeBag)
        
        tableView.rx
            .itemSelected
            .map { $0.row }
            .bind(onNext: viewModel.repositoryTapped)
            .addDisposableTo(disposeBag)
    }
    
    private func toggleSortTypeView(show: Bool) {
        view.layoutIfNeeded()
        sortTypeViewTopConstraint.constant = show ? 0 : -sortTypeView.frame.size.height
        
        UIView.animate(withDuration: 0.3) { [weak self] in
            self?.view.layoutIfNeeded()
        }
    }
}

extension SearchRepositoriesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.repositories.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: RepositoryTableViewCell.self),
                                                 for: indexPath) as! RepositoryTableViewCell
        let repository = viewModel.repositories.value[indexPath.row]
        cell.setup(with: repository)
        
        return cell
    }
}

extension SearchRepositoriesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return loadingFooterView
    }
}
