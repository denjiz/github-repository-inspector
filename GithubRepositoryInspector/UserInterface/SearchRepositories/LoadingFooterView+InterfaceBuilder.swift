//
//  LoadingFooterView+InterfaceBuilder.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/15/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import PureLayout

extension LoadingFooterView {
    
    func buildInterface() {
        activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        addSubview(activityIndicator)
        activityIndicator.autoCenterInSuperview()
        
        errorLabel = UILabel()
        addSubview(errorLabel)
        errorLabel.autoCenterInSuperview()
        errorLabel.autoMatch(.width, to: .width, of: self, withOffset: -100)
        errorLabel.numberOfLines = 0
        errorLabel.font = UIFont.systemFont(ofSize: 12)
        errorLabel.textAlignment = .center
        errorLabel.textColor = .red
    }
}
