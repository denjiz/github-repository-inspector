//
//  RepositoryTableViewCell+InterfaceBuilder.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/14/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import PureLayout

extension RepositoryTableViewCell {
    
    func buildInterface() {
        selectionStyle = .none
        
        rootView = UIView()
        contentView.addSubview(rootView)
        rootView.autoPinEdgesToSuperviewMargins()
        rootView.backgroundColor = UIColor(white: 0.9, alpha: 1)
        
        ownerImageView = HanekeImageView()
        rootView.addSubview(ownerImageView)
        ownerImageView.autoPinEdge(.left, to: .left, of: rootView, withOffset: 10)
        ownerImageView.autoPinEdge(.top, to: .top, of: rootView, withOffset: 10)
        ownerImageView.autoSetDimensions(to: CGSize(width: 70, height: 70))
        ownerImageView.backgroundColor = .white
        
        ownerUsernameLabel = UILabel()
        rootView.addSubview(ownerUsernameLabel)
        ownerUsernameLabel.autoPinEdge(.left, to: .right, of: ownerImageView, withOffset: 10)
        ownerUsernameLabel.autoPinEdge(.bottom, to: .bottom, of: ownerImageView, withOffset: -10)
        
        repositoryNameLabel = UILabel()
        rootView.addSubview(repositoryNameLabel)
        repositoryNameLabel.autoPinEdge(.left, to: .right, of: ownerImageView, withOffset: 10)
        repositoryNameLabel.autoPinEdge(toSuperviewEdge: .right, withInset: 10)
        repositoryNameLabel.autoPinEdge(.top, to: .top, of: ownerImageView, withOffset: 10)
        repositoryNameLabel.lineBreakMode = .byTruncatingTail
        
        infoStackView = UIStackView()
        rootView.addSubview(infoStackView)
        infoStackView.autoPinEdge(toSuperviewEdge: .left, withInset: 10)
        infoStackView.autoPinEdge(.top, to: .bottom, of: ownerImageView, withOffset: 10)
        infoStackView.autoPinEdge(.bottom, to: .bottom, of: rootView, withOffset: -10)
        infoStackView.axis = .vertical
        infoStackView.distribution = .equalSpacing
        infoStackView.spacing = 5
        infoStackView.alignment = .leading
        
        watchersLabel = UILabel()
        infoStackView.addArrangedSubview(watchersLabel)
        
        forksLabel = UILabel()
        infoStackView.addArrangedSubview(forksLabel)
        
        openIssuesLabel = UILabel()
        infoStackView.addArrangedSubview(openIssuesLabel)
        
        dateUpdatedLabel = UILabel()
        infoStackView.addArrangedSubview(dateUpdatedLabel)
    }
}
