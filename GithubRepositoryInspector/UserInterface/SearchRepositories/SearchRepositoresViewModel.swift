//
//  SearchRepositoresViewModel.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/14/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import RxSwift

class SearchRepositoriesViewModel {
    
    let repositories = Variable([Repository]())
    let sortType = Variable(SortType.stars)
    let isLoadingNewRepositories = Variable(false)
    let isLoadingMoreRepositories = Variable(false)
    let newRepositoriesLoadingError = Variable<String?>(nil)
    let moreRepositoriesLoadingError = Variable<String?>(nil)
    
    private let errorMessage = "Something went wrong. Please try again later."
    private let disposeBag = DisposeBag()
    private let dependencies: Dependencies
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    var isLoading: Bool {
        return isLoadingNewRepositories.value || isLoadingMoreRepositories.value
    }
    
    func changeSortTypeTapped() {
        dependencies.navigationService
            .presentSortTypeAlertController()
            .subscribe(onNext: { [weak self] sortType in
                self?.sortType.value = sortType
            })
            .addDisposableTo(disposeBag)
    }
    
    func searchRequested(for query: String) {
        guard !isLoading else { return }
        
        clearErrors()
        repositories.value = []
        isLoadingNewRepositories.value = true
        
        dependencies.apiController
            .repositories(for: query, sortType: sortType.value)
            .subscribe(
                onNext: { [weak self] repositories in
                    self?.repositories.value = repositories
                    self?.isLoadingNewRepositories.value = false
                },
                onError: { [weak self] error in
                    self?.isLoadingNewRepositories.value = false
                    self?.newRepositoriesLoadingError.value = self?.errorMessage
                }
            )
            .addDisposableTo(disposeBag)
    }
    
    func scrolledToEndOfList() {
        guard
            !repositories.value.isEmpty &&
            !isLoading &&
            dependencies.apiController.canLoadMore
        else { return }
        
        clearErrors()
        isLoadingMoreRepositories.value = true
        
        dependencies.apiController
            .loadMore()
            .subscribe(
                onNext: { [weak self] repositories in
                    self?.repositories.value.append(contentsOf: repositories)
                    self?.isLoadingMoreRepositories.value = false
                },
                onError: { [weak self] error in
                    self?.isLoadingMoreRepositories.value = false
                    self?.moreRepositoriesLoadingError.value = self?.errorMessage
                }
            )
            .addDisposableTo(disposeBag)
    }
    
    func repositoryTapped(at index: Int) {
        let repository = repositories.value[index]
        dependencies.navigationService.pushRepositoryDetailsViewController(for: repository)
    }
    
    private func clearErrors() {
        newRepositoriesLoadingError.value = nil
        moreRepositoriesLoadingError.value = nil
    }
}

extension SearchRepositoriesViewModel {
    
    struct Dependencies {
        let navigationService: NavigationService
        let apiController: APIController
    }
}
