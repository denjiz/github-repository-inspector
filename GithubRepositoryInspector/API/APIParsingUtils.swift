//
//  APIParsingUtils.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/14/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import Foundation

typealias JSONDictionary = [String : AnyObject]
typealias JSONArray = [JSONDictionary]

class APIParsingUtils {
    
    static func value<T>(from dictionary: JSONDictionary, for key: String) throws -> T {
        if let value = dictionary[key] as? T {
            return value
        }
        throw APIError.invalidJsonValue
    }
    
    static func date(from dictionary: JSONDictionary, for key: String) throws -> Date {
        if
            let dateString = dictionary[key] as? String,
            let date = DateFormatters.iso8601dateFormatter.date(from: dateString)
        {
            return date
        }
        throw APIError.invalidJsonValue
    }
    
    static func nextPageUrl(from response: HTTPURLResponse) -> URL? {
        guard let links = response
            .allHeaderFields[HTTPConstants.HeaderField.link]
            as? String else { return nil }
        
        let regexPattern = "<([^,>]*)>; rel=\"([^,\"]*)\""
        guard let regex = try? NSRegularExpression(pattern: regexPattern, options: []) else { return nil }
        
        let nsLinks = links as NSString
        let matches = regex.matches(in: links, options: [], range: NSMakeRange(0, nsLinks.length))
        for match in matches {
            let relValue = nsLinks.substring(with: match.rangeAt(2))
            if relValue == "next" {
                let nextPageUrlString = nsLinks.substring(with: match.rangeAt(1))
                return URL(string: nextPageUrlString)
            }
        }
        
        return nil
    }
}
