//
//  Repository.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/14/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import Foundation

struct Repository {
    
    let name: String
    let description: String
    let urlString: String
    let dateCreated: Date
    let dateUpdated: Date
    let language: String
    let numberOfWatchers: Int
    let numberOfForks: Int
    let numberOfOpenIssues: Int
    let owner: RepositoryOwner
    
    init(from json: JSONDictionary) throws {
        name = try APIParsingUtils.value(from: json, for: "name")
        description = try APIParsingUtils.value(from: json, for: "description")
        urlString = try APIParsingUtils.value(from: json, for: "html_url")
        dateCreated = try APIParsingUtils.date(from: json, for: "created_at")
        dateUpdated = try APIParsingUtils.date(from: json, for: "updated_at")
        language = try APIParsingUtils.value(from: json, for: "language")
        numberOfWatchers = try APIParsingUtils.value(from: json, for: "watchers_count")
        numberOfForks = try APIParsingUtils.value(from: json, for: "forks_count")
        numberOfOpenIssues = try APIParsingUtils.value(from: json, for: "open_issues_count")
        
        let ownerJson: JSONDictionary = try APIParsingUtils.value(from: json, for: "owner")
        owner = try RepositoryOwner(from: ownerJson)
    }
}
