//
//  HTTPConstants.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/16/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import Foundation

struct HTTPConstants {
    
    struct HeaderField {
        static let accept = "Accept"
        static let link = "Link"
    }
}
