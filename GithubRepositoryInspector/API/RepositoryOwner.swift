//
//  RepositoryOwner.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/14/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import Foundation

struct RepositoryOwner {
    
    let username: String
    let urlString: String
    let avatarUrlString: String
    
    init(from json: JSONDictionary) throws {
        username = try APIParsingUtils.value(from: json, for: "login")
        urlString = try APIParsingUtils.value(from: json, for: "html_url")
        avatarUrlString = try APIParsingUtils.value(from: json, for: "avatar_url")
    }
}
