//
//  APIController.swift
//  GithubRepositoryInspector
//
//  Created by Denis Mendica on 7/14/17.
//  Copyright © 2017 Denis Mendica. All rights reserved.
//

import RxSwift

enum SortType: String {
    case stars
    case forks
    case updated
    
    static var allValues: [SortType] {
        return [.stars, .forks, .updated]
    }
}

enum APIError: Error {
    case couldNotConstructUrl
    case invalidResponseData
    case invalidJsonValue
}

class APIController {
    
    private let baseUrl = "https://api.github.com"
    private let dependencies: Dependencies
    private var nextPageUrl: URL?
    
    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    var canLoadMore: Bool {
        return nextPageUrl != nil
    }
    
    func repositories(for query: String, sortType: SortType) -> Observable<[Repository]> {
        let path = "/search/repositories"
        let query = query.replacingOccurrences(of: " ", with: "+")
        let params = "?q=\(query)&sort=\(sortType)"
        
        let urlString = "\(baseUrl)\(path)\(params)"
        guard let url = URL(string: urlString) else {
            return Observable.error(APIError.couldNotConstructUrl)
        }
        
        return repositories(from: url)
    }
    
    func loadMore() -> Observable<[Repository]> {
        guard let nextPageUrl = nextPageUrl else {
            return Observable.just([])
        }
        return repositories(from: nextPageUrl)
    }
    
    private func repositories(from url: URL) -> Observable<[Repository]> {
        var request = URLRequest(url: url)
        request.addValue("application/vnd.github.v3+json",
                         forHTTPHeaderField: HTTPConstants.HeaderField.accept)
        
        return dependencies.urlSession.rx
            .response(request: request)
            .map { [weak self] response, data in
                guard let `self` = self else { return [] }
                
                let responseJson = try self.jsonDictionary(from: data)
                let repositoriesJson: JSONArray = try APIParsingUtils.value(from: responseJson, for: "items")
                let repositories = repositoriesJson.flatMap { json in
                    try? Repository(from: json)
                }
                
                self.nextPageUrl = APIParsingUtils.nextPageUrl(from: response)
                
                return repositories
            }
            .observeOn(MainScheduler.instance)
    }
    
    private func jsonDictionary(from responseData: Data) throws -> JSONDictionary {
        if
            let jsonObject = try? JSONSerialization.jsonObject(with: responseData, options: []),
            let jsonDictonary = jsonObject as? JSONDictionary
        {
            return jsonDictonary
        }
        throw APIError.invalidResponseData
    }
}

extension APIController {
    
    struct Dependencies {
        let urlSession: URLSession
    }
}
